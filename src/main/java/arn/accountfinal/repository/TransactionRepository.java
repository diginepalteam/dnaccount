package arn.accountfinal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import arn.accountfinal.model.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

}
