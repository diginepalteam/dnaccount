package com.diginepal.schoolmgmt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.diginepal.schoolmgmt.model.AccountHead;
import com.diginepal.schoolmgmt.model.AccountType;
import com.diginepal.schoolmgmt.service.AccountHeadService;
@Controller
public class AccountHeadController {
	@Autowired
	private AccountHeadService accountheadservice;

			
	// display list of Account Heads
    @GetMapping("/")
    public String viewHomePage(Model model) {
      //  model.addAttribute("listAccounthead", accountheadservice.getallAccountHeads());
        //For Test purpose only
       // System.out.println("list"+accountheadservice.getallAccountHeads());
      //  return "index";
    	return findPaginated(1, "id", "asc", model);
    }
    
    @GetMapping("/NewAccountForm")
    public String showNewAccountHeadForm(Model model) {
        // create model attribute to bind form data
        AccountHead accountHead = new AccountHead();
        model.addAttribute("accountHead", accountHead);
      
        accountHead.getAccount_Head();
        model.addAttribute("acchead",AccountType.values());
        return "AddaccHead";
    }
    
    @RequestMapping(path = "/GetCities", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public List<AccountHead> findCities() {

        var cities = (List<AccountHead>) accountheadservice.getallAccountHeads();

        return cities;
    }
//    @RequestMapping(value = "/accounthead", method = RequestMethod.GET)
//    public List<AccountHead> getaccString ( @RequestParam long employeeId) {
//    	 var cities = (List<AccountHead>) accountheadservice.getallAccountHeads();
//    	 return cities;
//     // code to get employee by employee id
//    }
//    
//    @GetMapping("/GetCities")
//	public @ResponseBody String findCities(@RequestParam Integer accid)
//	{
//		String json = null;
//		List<AccountHead> list = accountheadservice.getallAccountHeads();
//		try {
//			cities = new ObjectMapper().writeValueAsString(list);
//		} catch (JsonProcessingException e) {
//			e.printStackTrace();
//		}
//		return cities;
//	}
    
//    **************************using Javascript*******************
//    @RequestMapping(value = "/displayaddphone")
//    public String appendaddphone(@RequestParam(value="count") int addphonecount,ModelMap model){
//        // define "contact" object, suppose for example
////        Contact contact = populateContact();
//        model.addAttribute("contact", contact);
//        return "addnewphone";
//    @RequestMapping(value="myMealPlan", method=RequestMethod.GET)
//    public void init(ModelMap map, HttpServletRequest request){
//    System.out.println("Bye method called..!");
//    }
    
//    @ResponseBody
//    @RequestMapping("/country", method = RequestMethod.GET, produces = "application/json")
//    public Map<String, String> getCountry(@PathVariable String latitude, @PathVariable 
//    String longitude){

//    final Map<String, String> messageObject = new HashMap<>();
//    messageObject.put("latitude", latitude);
//    messageObject.put("longitude", longitude);
//
//    //return the html
//    return messageObject;
//    }
//    @PostMapping("/getSubcodeValues")
//    public @ResponseBody
//    Map<String, String> getStateCityValues(@RequestParam("subcodehead") String subcodehead) {
//        Map<String, String>subcodeValues = new HashMap<>();
//
//        List<AccountHead> accountHeads = accountheadservice.findByGroupcode(subcodehead);
//
//        for(AccountHead accountHead : accountHeads){
//        	subcodeValues.put(String.valueOf(accountHead.getSub_Code()), accountHead.getSub_Account_Head());
//        }
//
//        return subcodeValues;  
//    }
    
//  **************************using Javascript*******************
    
    
////*********************For Ajax *******************
//  *********************For Ajax *******************
//  @ResponseBody
//	@RequestMapping(value = "loadSubcodeByAccHead/{id}", method = RequestMethod.GET)
//	public String loadStatesByCountry(@PathVariable("id") String id) {
//		Gson gson = new Gson();
//		return gson.toJson(accountheadservice.findBySubcode(id));
//	}
//	@ResponseBody
//	@RequestMapping(value = "loadGroupcodeBysubcode/{id}", method = RequestMethod.GET)
//	public String loadCitiesByState(@PathVariable("id") String id) {
//		Gson gson = new Gson();
//		return gson.toJson(accountheadservice.findByGroupcode(id));
//	}
//
//    @ResponseBody
//	@RequestMapping(value = "loadStatesByCountry/{id}", method = RequestMethod.GET)
//	public String loadStatesByCountry(@PathVariable("id") int id , Model model) {
//    	 
//               // get employee from the service
//    		 AccountHead accountHead = accountheadservice.getAccountHeadById(id);
//    		 System.out.println("Id Shown Below:"+id);
////    		  //System.out.println("list out arun"+accountheadservice.getAccountHeadById(id));
////    		 // set employee as a model attribute to pre-populate the form
//   		 model.addAttribute("accountHeads", accountHead);
// 	 
//		return "redirect:/new_accountHead";
//	}

//
//	@ResponseBody
//	@RequestMapping(value = "loadCitiesByState/{id}", method = RequestMethod.GET)
//	public String loadCitiesByState(@PathVariable("id") int id) {
//		Gson gson = new Gson();
//		return gson.toJson(cityService.findByState(id));
//	}
	
////*********************For Ajax *******************
    
    
    @PostMapping("/saveAccountHead")
    public String saveAccountHead(@ModelAttribute("accountHead") AccountHead accountHead) {
        // save account to database
    	accountheadservice.saveAccountHeads(accountHead);
        return "redirect:/";
    }
   
    @GetMapping("/showFormForUpdate/{id}")
	public String showFormForUpdate(@PathVariable ( value = "id") long id, Model model) {
	 
	 // get employee from the service
	 AccountHead accountHead = accountheadservice.getAccountHeadById(id);
	 
	 //Get Account Type 
     accountHead.getAccount_Head();
     model.addAttribute("acchead",AccountType.values());
	 model.addAttribute("accountHead", accountHead);
	 return "update_accounthead";
	}
   
    @GetMapping("/showFormForDelete/{id}")
   	public String showFormForDelete(@PathVariable ( value = "id") long id, Model model) {
   	 
   	 // Call delete method from the service
   	
   	this.accountheadservice.deleteAccountHeadbyId(id);
   	 return "redirect:/";
   	}
    
    
//    Let's change the existing method to provide a support for sorting:
    //page/1?sortField=name&sortDir=asc
    @GetMapping("/page/{pageNo}")
    public String findPaginated(@PathVariable(value = "pageNo") int pageNo,
        @RequestParam("sortField") String sortField,
        @RequestParam("sortDir") String sortDir,
        Model model) {
        int pageSize = 5;

        Page < AccountHead > page = accountheadservice.findPaginated(pageNo, pageSize, sortField, sortDir);
        List < AccountHead > listAccounthead = page.getContent();

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");

        model.addAttribute("listAccounthead", listAccounthead);
        return "index";
    }
 
}
