package arn.accountfinal.service;

import java.util.List;

import arn.accountfinal.model.Transaction;

public interface TransactionService {
	List<Transaction> getallTransactions();
	void saveTransactions(Transaction transaction);
	Transaction getTransactionById(long id);
	void deleteTransactionbyId(long id);
}
