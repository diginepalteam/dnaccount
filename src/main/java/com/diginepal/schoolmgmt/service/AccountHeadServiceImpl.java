package com.diginepal.schoolmgmt.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.diginepal.schoolmgmt.model.AccountHead;
import com.diginepal.schoolmgmt.repository.AccountHeadRepository;

@Service
public class AccountHeadServiceImpl implements AccountHeadService {

    @Autowired
    private AccountHeadRepository accountHeadRepository;

   
	@Override
	public List<AccountHead> getallAccountHeads() {
		// TODO Auto-generated method stub
		return accountHeadRepository.findAll();
	}

	@Override
	public List<AccountHead> findBySubcode(String id) {
		// TODO Auto-generated method stub
		return accountHeadRepository.findBySubcode(id);
	}

	@Override
	public List<AccountHead> findByGroupcode(String id) {
		// TODO Auto-generated method stub
		 return accountHeadRepository.findByGroupcode(id);
	}


	@Override
	public void saveAccountHeads(AccountHead accountHead) {
		// TODO Auto-generated method stub
		this.accountHeadRepository.save(accountHead);
	}

	@Override
	public AccountHead getAccountHeadById(long id) {
		// TODO Auto-generated method stub
		Optional < AccountHead > optional = accountHeadRepository.findById(id);
	    AccountHead accountHead = null;
	    if (optional.isPresent()) {
	        accountHead = optional.get();
	    } else {
	        throw new RuntimeException(" Account Head not found for id :: " + id);
	    }
	    return accountHead;
	}

	@Override
	public void deleteAccountHeadbyId(long id) {
		// TODO Auto-generated method stub
		this.accountHeadRepository.deleteById(id);
		
	}

//	@Override
//	public Page<AccountHead> findPaginated(int pageNo, int pageSize) {
//		PageRequest pageable = PageRequest.of(pageNo - 1, pageSize);
//			return this.accountHeadRepository.findAll(pageable);
//	}
//	Add below method to EmployeeServiceImpl class:
	
	@Override
	public Page<AccountHead> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
		// TODO Auto-generated method stub
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
            Sort.by(sortField).descending();

        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
		return this.accountHeadRepository.findAll(pageable);
	}

	
	

		
}