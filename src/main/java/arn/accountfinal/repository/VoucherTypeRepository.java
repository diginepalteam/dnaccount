package arn.accountfinal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import arn.accountfinal.model.VoucherType;

public interface VoucherTypeRepository extends JpaRepository<VoucherType, Long> {
	
}
