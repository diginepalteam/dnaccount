package com.diginepal.schoolmgmt.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity(name="tbl_accounthead")
@Table(name = "tbl_accounthead")
public class AccountHead {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
     
    @Column(name = "Main_Code")
    private long Main_Code;
    
    @Column(name = "Account_Head")
//    private String Account_Head;
    private AccountType Account_Head;
    
    @Column(name = "Sub_Code")
    private long Sub_Code;
    
    @Column(name = "Sub_Account_Head")
    private String Sub_Account_Head;
    
    @Column(name = "Group_Code")
    private long Group_Code;
    
    @Column(name = "Group_head")
    private String Group_head;
    
    @Column(name = "Categroy_Code")
    private long Categroy_Code;
    
    @Column(name = "Category_Head")
    private String Category_Head;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getMain_Code() {
		return Main_Code;
	}

	public void setMain_Code(long main_Code) {
		Main_Code = main_Code;
	}

	public AccountType getAccount_Head() {
		return Account_Head;
	}

	public void setAccount_Head(AccountType account_Head) {
		Account_Head = account_Head;
	}

	public long getSub_Code() {
		return Sub_Code;
	}

	public void setSub_Code(long sub_Code) {
		Sub_Code = sub_Code;
	}

	public String getSub_Account_Head() {
		return Sub_Account_Head;
	}

	public void setSub_Account_Head(String sub_Account_Head) {
		Sub_Account_Head = sub_Account_Head;
	}

	public long getGroup_Code() {
		return Group_Code;
	}

	public void setGroup_Code(long group_Code) {
		Group_Code = group_Code;
	}

	public String getGroup_head() {
		return Group_head;
	}

	public void setGroup_head(String group_head) {
		Group_head = group_head;
	}

	public long getCategroy_Code() {
		return Categroy_Code;
	}

	public void setCategroy_Code(long categroy_Code) {
		Categroy_Code = categroy_Code;
	}

	public String getCategory_Head() {
		return Category_Head;
	}

	public void setCategory_Head(String category_Head) {
		Category_Head = category_Head;
	}

	public AccountHead(long id, long main_Code, AccountType account_Head, long sub_Code, String sub_Account_Head,
			long group_Code, String group_head, long categroy_Code, String category_Head) {
		super();
		this.id = id;
		Main_Code = main_Code;
		Account_Head = account_Head;
		Sub_Code = sub_Code;
		Sub_Account_Head = sub_Account_Head;
		Group_Code = group_Code;
		Group_head = group_head;
		Categroy_Code = categroy_Code;
		Category_Head = category_Head;
	}

	public AccountHead() {
		// TODO Auto-generated constructor stub
	}

	public static Object values() {
		// TODO Auto-generated method stub
		return null;
	}

	
		
    
}