package arn.accountfinal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "Transaction")
public class Transaction {
	
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Long id;
	 //unique = true, nullable=false 

	    @Column(name = "Transaction_ID",unique=true,nullable=false)	
        private Long	Transaction_ID;
	    
	    @Column(name = "Transaction_Date")
	    @DateTimeFormat(pattern = "yyyy-MM-dd")
	    private String	Transaction_Date;
	    
	    @Column(name = "Transc_type",nullable=false)	
        private int	Transc_type;
	    
	    @Column(name = "Transc_Entryno",nullable=false)	
        private int	Transc_Entryno;
	    
	    @Column(name = "category_code",nullable=false)	
        private Long	category_code;
        
	    @Column(name = "Transac_Desc",length = 1024)
	    private String	Transac_Desc;
	    
	    @Column(name = "Transac_Dr",nullable=false)
        private Long	Transac_Dr;
	    
	    @Column(name = "Transac_Cr",nullable=false)
        private Long	Transac_Cr;
	    
	    @Column(name = "Transac_fy")
        private String	Transac_fy;
	    
	    @Column(name = "Transac_userid")
        private String	Transac_userid;
	    
	    
	    
	    public Transaction(Long transaction_ID, String transaction_Date, int transc_type, int transc_Entryno,
				Long category_code, String transac_Desc, Long transac_Dr, Long transac_Cr, String transac_fy,
				String transac_userid) {
			super();
			Transaction_ID = transaction_ID;
			Transaction_Date = transaction_Date;
			Transc_type = transc_type;
			Transc_Entryno = transc_Entryno;
			this.category_code = category_code;
			Transac_Desc = transac_Desc;
			Transac_Dr = transac_Dr;
			Transac_Cr = transac_Cr;
			Transac_fy = transac_fy;
			Transac_userid = transac_userid;
		}

		public Transaction() {
			// TODO Auto-generated constructor stub
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public Long getTransaction_ID() {
			return Transaction_ID;
		}

		public void setTransaction_ID(Long transaction_ID) {
			Transaction_ID = transaction_ID;
		}

		public String getTransaction_Date() {
			return Transaction_Date;
		}

		public void setTransaction_Date(String transaction_Date) {
			Transaction_Date = transaction_Date;
		}

		public int getTransc_type() {
			return Transc_type;
		}

		public void setTransc_type(int transc_type) {
			Transc_type = transc_type;
		}

		public int getTransc_Entryno() {
			return Transc_Entryno;
		}

		public void setTransc_Entryno(int transc_Entryno) {
			Transc_Entryno = transc_Entryno;
		}

		public Long getCategory_code() {
			return category_code;
		}

		public void setCategory_code(Long category_code) {
			this.category_code = category_code;
		}

		public String getTransac_Desc() {
			return Transac_Desc;
		}

		public void setTransac_Desc(String transac_Desc) {
			Transac_Desc = transac_Desc;
		}

		public Long getTransac_Dr() {
			return Transac_Dr;
		}

		public void setTransac_Dr(Long transac_Dr) {
			Transac_Dr = transac_Dr;
		}

		public Long getTransac_Cr() {
			return Transac_Cr;
		}

		public void setTransac_Cr(Long transac_Cr) {
			Transac_Cr = transac_Cr;
		}

		public String getTransac_fy() {
			return Transac_fy;
		}

		public void setTransac_fy(String transac_fy) {
			Transac_fy = transac_fy;
		}

		public String getTransac_userid() {
			return Transac_userid;
		}

		public void setTransac_userid(String transac_userid) {
			Transac_userid = transac_userid;
		}

		

}
