//package arn.accountfinal.service;
//
//import java.util.List;
//import java.util.Optional;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import arn.accountfinal.model.AccountType;
//import arn.accountfinal.repository.AccountTypeRepository;
//
//@Service
//public class AccountTypeServiceImpl implements AccountTypeService {
//
//	@Autowired
//	private AccountTypeRepository accountTypeRepository;
//
//	@Override
//	public List<AccountType> getallAccountTypes() {
//		// TODO Auto-generated method stub
//		return accountTypeRepository.findAll();
//	}
//
//	@Override
//	public void saveAccountTypes(AccountType accountType) {
//		// TODO Auto-generated method stub
//		this.accountTypeRepository.save(accountType);
//	}
//
//	@Override
//	public AccountType getAccountTypeById(long id) {
//		// TODO Auto-generated method stub
//		Optional<AccountType> optional = accountTypeRepository.findById(id);
//		AccountType accountType = null;
//		if (optional.isPresent()) {
//			accountType = optional.get();
//		} else {
//			throw new RuntimeException(" Account Type not found for id :: " + id);
//		}
//		return accountType;
//	}
//
//	@Override
//	public void deleteAccountTypebyId(long id) {
//		// TODO Auto-generated method stub
//		this.accountTypeRepository.deleteById(id);
//
//	}
//
//}