package arn.accountfinal.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import arn.accountfinal.model.VoucherType;
import arn.accountfinal.repository.VoucherTypeRepository;

@Service
public class VoucherTypeServiceImpl implements VouchertypeService {

    @Autowired
    private VoucherTypeRepository voucherTypeRepository;

   
	@Override
	public List<VoucherType> getallVoucherTypes() {
		// TODO Auto-generated method stub
		return voucherTypeRepository.findAll();
	}


//	@Override
//	public void saveVoucherTypes(VoucherType voucherType) {
//		// TODO Auto-generated method stub
//		this.voucherTypeRepository.save(voucherType);
//	}

	@Override
	public VoucherType getVoucherTypeById(long id) {
		// TODO Auto-generated method stub
		Optional < VoucherType > optional = voucherTypeRepository.findById(id);
	    VoucherType voucherType = null;
	    if (optional.isPresent()) {
	        voucherType = optional.get();
	    } else {
	        throw new RuntimeException(" Account Head not found for id :: " + id);
	    }
	    return voucherType;
	}

	@Override
	public void deleteVoucherTypebyId(long id) {
		// TODO Auto-generated method stub
		this.voucherTypeRepository.deleteById(id);
		
	}


	@Override
	public void createOrUpdateVoucherType(VoucherType voucherType) {
		// TODO Auto-generated method stub
		this.voucherTypeRepository.save(voucherType);
	}


		
}