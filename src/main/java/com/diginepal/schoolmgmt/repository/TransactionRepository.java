package com.diginepal.schoolmgmt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.diginepal.schoolmgmt.model.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

}
