package com.diginepal.schoolmgmt.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.diginepal.schoolmgmt.model.AccountHead;

public interface AccountHeadService {
	List<AccountHead> getallAccountHeads();
	void saveAccountHeads(AccountHead accountHead);
	AccountHead getAccountHeadById(long id);
	void deleteAccountHeadbyId(long id);
	
	
	
//	To use paging and sorting APIs provided by Spring Data JPA, your repository interface must extend 
//	the PagingAndSortingRepository interface which defines the following couple of methods (T refers to an entity class)
//	JpaRepository interface extends PagingAndSortingRepository interface so if your repository interface is of type JpaRepository, you don’t have to make a change to it.
//	For example, use the following to get the first page from the database, with 5 items per page:
//	int pageNumber = 1;
//	int pageSize = 5;
//	Pageable pageable = PageRequest.of(pageNumber, pageSize);
//	 
//	Page<Product> page = repository.findAll(pageable);
//	Then you can get the actual content as follows:
//		List<Employee> listEmployees = page.getContent();
//		With a Page object you can know the total rows in the database and the total pages according to the given page size:
//		long totalItems = page.getTotalElements();
//		int totalPages = page.getTotalPages();
//	Add below method to this interface:
	
	//Page<AccountHead> findPage (int pageNo, int pageSize);
	
//	This will sort the result by fieldName in ascending order. fieldName must match a field name declared in the entity class. 
//	We can also sort by more than one field, for example:
//	
	Page < AccountHead > findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);
	List<AccountHead> findByGroupcode(String id);
	List<AccountHead> findBySubcode(String id);
	//List<AccountHead> getAccountHeadById();
	
}
