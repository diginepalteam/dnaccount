package arn.accountfinal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import arn.accountfinal.model.AccountHead;
import arn.accountfinal.model.AccountType;
import arn.accountfinal.service.AccountHeadService;

@Controller
public class AccountHeadController {
	
	@Autowired
	
	private AccountHeadService accountheadservice;

			
	// display list of Account Heads
    @GetMapping("/")
    public String viewHomePage(Model model) {
      //  model.addAttribute("listAccounthead", accountheadservice.getallAccountHeads());
        //For Test purpose only
       // System.out.println("list"+accountheadservice.getallAccountHeads());
      //  return "index";
    	return findPaginated(1, "id", "asc", model);
    }
    
    @GetMapping("/NewAccountForm")
    public String showNewAccountHeadForm(Model model) {
        // create model attribute to bind form data
        AccountHead accountHead = new AccountHead();
        model.addAttribute("accountHead", accountHead);
        
        //Get Account Type 
        accountHead.getAccount_Head();
        model.addAttribute("acchead",AccountType.values());
       // System.out.println("list"+accountHead.getAccount_Head());
        return "new_accountHead";
    }
    
    @PostMapping("/saveAccountHead")
    public String saveAccountHead(@ModelAttribute("accountHead") AccountHead accountHead) {
        // save employee to database
    	accountheadservice.saveAccountHeads(accountHead);
        return "redirect:/";
    }
   
    @GetMapping("/showFormForUpdate/{id}")
	public String showFormForUpdate(@PathVariable ( value = "id") long id, Model model) {
	 
	 // get employee from the service
	 AccountHead accountHead = accountheadservice.getAccountHeadById(id);
	  //System.out.println("Id"+id);
	  //System.out.println("list out arun"+accountheadservice.getAccountHeadById(id));
	 // set employee as a model attribute to pre-populate the form
	 //Get Account Type 
     accountHead.getAccount_Head();
     model.addAttribute("acchead",AccountType.values());
	 model.addAttribute("accountHead", accountHead);
	 return "update_accounthead";
	}
   
    @GetMapping("/showFormForDelete/{id}")
   	public String showFormForDelete(@PathVariable ( value = "id") long id, Model model) {
   	 
   	 // Call delete method from the service
   	
   	this.accountheadservice.deleteAccountHeadbyId(id);
   	 return "redirect:/";
   	}
    
    
//    Let's change the existing method to provide a support for sorting:
    //page/1?sortField=name&sortDir=asc
    @GetMapping("/page/{pageNo}")
    public String findPaginated(@PathVariable(value = "pageNo") int pageNo,
        @RequestParam("sortField") String sortField,
        @RequestParam("sortDir") String sortDir,
        Model model) {
        int pageSize = 5;

        Page < AccountHead > page = accountheadservice.findPaginated(pageNo, pageSize, sortField, sortDir);
        List < AccountHead > listAccounthead = page.getContent();

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");

        model.addAttribute("listAccounthead", listAccounthead);
        return "index";
    }
 
}
