package arn.accountfinal.service;

import java.util.List;

import arn.accountfinal.model.VoucherType;

public interface VouchertypeService {
	List<VoucherType> getallVoucherTypes();
	//void saveVoucherTypes(VoucherType voucherType);
	VoucherType getVoucherTypeById(long id);
	void deleteVoucherTypebyId(long id);
	void createOrUpdateVoucherType(VoucherType vouchertype);
}
