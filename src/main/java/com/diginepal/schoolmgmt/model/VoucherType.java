package com.diginepal.schoolmgmt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VoucherType")
public class VoucherType {
	  @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;
	   //unique = true, nullable=false 
	    @Column(name = "Voucher_type",nullable=false)
	    private String Voucher_type;
	    @Column(name = "description",length = 128)
	    private String description;
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getVoucher_type() {
			return Voucher_type;
		}
		public void setVoucher_type(String voucher_type) {
			Voucher_type = voucher_type;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public VoucherType(String voucher_type, String description) {
			super();
			Voucher_type = voucher_type;
			this.description = description;
		}
		public VoucherType() {
			super();
			// TODO Auto-generated constructor stub
		}
	    
}
