//package arn.accountfinal.controller;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//
//import arn.accountfinal.model.AccountType;
//import arn.accountfinal.service.AccountTypeService;
//
//@Controller
//public class AccountTypeControl {
//	@Autowired
//	private AccountTypeService accountTypeService;
//	
//	// display list of Account Heads
//    @GetMapping("/AccountSetup")
//    public String viewHomePage(Model model) {
//       model.addAttribute("listAccountType", accountTypeService.getallAccountTypes());
//       //For Test purpose only
//       System.out.println("list"+accountTypeService.getallAccountTypes());
//      return "accountsetup";
//    }
//    
//    @GetMapping("/NewAccountTypeForm")
//    public String showNewAccountTypeForm(Model model) {
//        // create model attribute to bind form data
////        AccountType accountType = new AccountType();
////        model.addAttribute("accountType", accountType);
//       // System.out.println("list"+accountType.getAccount_Head());
//        return "new_accountType";
//    }
//    
//    @PostMapping("/saveAccountType")
//    public String saveAccountType(@ModelAttribute("accountType") AccountType accountType) {
//        // save employee to database
//    	accountTypeService.saveAccountTypes(accountType);
//        return "redirect:/Voucher-Account-Type";
//    }
//   
//    @GetMapping("/AccountTypeForUpdate/{id}")
//	public String showFormForUpdate(@PathVariable ( value = "id") long id, Model model) {
//	 
//	 // get employee from the service
//	 AccountType accountType = accountTypeService.getAccountTypeById(id);
//	  //System.out.println("Id"+id);
//	  //System.out.println("list out arun"+accountTypeService.getaccountTypeById(id));
//	 // set employee as a model attribute to pre-populate the form
//	 model.addAttribute("accountType", accountType);
//	 return "update_accountType";
//	}
//   
//    @GetMapping("/AccountTypeForDelete/{id}")
//   	public String showFormForDelete(@PathVariable ( value = "id") long id, Model model) {
//   	 
//   	 // Call delete method from the service
//   	this.accountTypeService.deleteAccountTypebyId(id);
//   	 return "redirect:/Voucher-Account-Type";
//   	}
//    
//}
