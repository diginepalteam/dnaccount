package com.diginepal.schoolmgmt.service;

import java.util.List;

import com.diginepal.schoolmgmt.model.Transaction;

public interface TransactionService {
	List<Transaction> getallTransactions();
	void saveTransactions(Transaction transaction);
	Transaction getTransactionById(long id);
	void deleteTransactionbyId(long id);
}
