package com.diginepal.schoolmgmt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.diginepal.schoolmgmt.model.VoucherType;

public interface VoucherTypeRepository extends JpaRepository<VoucherType, Long> {
	
}
