package arn.accountfinal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import arn.accountfinal.model.AccountHead;

@Repository

public interface AccountHeadRepository extends JpaRepository<AccountHead, Long> {

}
