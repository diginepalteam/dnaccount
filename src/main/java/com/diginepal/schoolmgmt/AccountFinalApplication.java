package com.diginepal.schoolmgmt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountFinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountFinalApplication.class, args);
	}

}
