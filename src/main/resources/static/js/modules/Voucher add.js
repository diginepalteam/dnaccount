 $(document).ready(function () {
      var itm = [];
      // Add Button Click Function
      $('#add').click(function () {

          var i = 0;
          if ($('#itemName').val() == '') {
              $('#itemName').siblings('span.error').css('visibility', 'visible');
          } else {
              $('#itemName').siblings('span.error').css('visibility', 'hidden');
              i++;
          }

          if ($('#quantity').val() == '' || isNaN($('#quantity').val())) {
              $('#quantity').siblings('span.error').css('visibility', 'visible');

          } else {
              $('#quantity').siblings('span.error').css('visibility', 'hidden');
              i++;
          }

          if ($('#rate').val() == '' || isNaN($('#rate').val())) {
              $('#rate').siblings('span.error').css('visibility', 'visible');
          } else {
              $('#rate').siblings('span.error').css('visibility', 'hidden');
              i++;
          }
          if (i == 3) {
              itm.push({
                  ItemName: $('#itemName').val(),
                  Quantity: parseInt($('#quantity').val()),
                  Rate: parseFloat($('#rate').val()),
                  Total: parseInt($('#quantity').val()) * parseFloat($('#rate').val())
              });
              $('#itemName').val('').focus();
              $('#quantity').val('');
              $('#rate').val('');

          }
          GenratedItemTable(itm);
      });
      $('input[value="save"]').click(function () {
          $.ajax({

              url: ' /Home/SaveOrder/',
              type: "POST",
              data: JSON.stringify(itm),
              datatype: "JSON",
              ContentType: "application/json",
              success: function (d) {
                  //check is successfully save to database 
                  if (d.status == true) {
                      alert('SuccessFully Done');
                  }
                  else {
                      alert('Failed to Save Data');
                  }

              },
              error: function () {
                  alert('Error. Please Try Again');
              }
          });
      });
      // Function For Show Added Items in Table
      function GenratedItemTable(itm) {
          if (itm.length > 0) {
              var $table = $('<table/>');
              $table.append('<thead><tr><th>Item</th><th>Quantity</th><th>Rate</th><th>Total</th></tr></thead>');
              var $tbody = $('<tbody/>');
              $.each(itm, function (i, val) {
                  var $row = $('<tr/>');
                  $row.append($('<td/>').html(val.ItemName));
                  $row.append($('<td/>').html(val.Quantity));
                  $row.append($('<td/>').html(val.Rate));
                  $row.append($('<td/>').html(val.Total));
                  $tbody.append($row);

              });
              $table.append($tbody);
              $('#OrderItems').html($table);
          }
      }



  });