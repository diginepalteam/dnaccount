package com.diginepal.schoolmgmt.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diginepal.schoolmgmt.model.Transaction;
import com.diginepal.schoolmgmt.repository.TransactionRepository;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

   
	@Override
	public List<Transaction> getallTransactions() {
		// TODO Auto-generated method stub
		return transactionRepository.findAll();
	}

//
//	@Override
//	public List<AccountHead> saveAccountHeads() {
//		// TODO Auto-generated method stub
//		this.accountHeadRepository.save(AccountHead);
//	}


	@Override
	public void saveTransactions(Transaction transaction) {
		// TODO Auto-generated method stub
		this.transactionRepository.save(transaction);
	}

	@Override
	public Transaction getTransactionById(long id) {
		// TODO Auto-generated method stub
		Optional < Transaction > optional = transactionRepository.findById(id);
	    Transaction transaction = null;
	    if (optional.isPresent()) {
	    	transaction = optional.get();
	    } else {
	        throw new RuntimeException(" Account Head not found for id :: " + id);
	    }
	    return transaction;
	}

	@Override
	public void deleteTransactionbyId(long id) {
		// TODO Auto-generated method stub
		this.transactionRepository.deleteById(id);
		
	}

//
	

		
}