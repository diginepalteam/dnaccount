package arn.accountfinal.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import arn.accountfinal.model.VoucherType;
import arn.accountfinal.service.VouchertypeService;

@Controller
//@RequestMapping("/Voucher-Account-Type")
public class VoucherTypeControl {
	
	   
    //***********************************************************For Vouchet Type************************************************************
    
    @Autowired
    private VouchertypeService vouchertypeService;
    
    @RequestMapping("/VoucherType")
	public String showNewVoucherTypeForm(Model model) 
	{	
		System.out.println("getAllVoucherTypes");
		
		List<VoucherType> list = vouchertypeService.getallVoucherTypes();

		model.addAttribute("voucherTypes", list);
		
		return "vouchertype";
	}
	
	
	@RequestMapping(path = {"/AddvoucherType", "/edit/{id}"})
	public String getVoucherTypeById(Model model, @PathVariable("id") Optional<Long> id) 
							throws RecordNotFoundException	{
		
		System.out.println("edit Voucher Type ById" + id);
		if (id.isPresent()) {
			VoucherType entity = vouchertypeService.getVoucherTypeById(id.get());
			model.addAttribute("vouchertype", entity);
		} else {
			model.addAttribute("vouchertype", new VoucherType());
		}
		
		
		return "createvouchertype";
	}
	
	@RequestMapping(path = "/delete/{id}")
	public String deleteVoucherTypeById(Model model, @PathVariable("id") Long id) 
							throws RecordNotFoundException 
	{
		
		System.out.println("delete Voucher Type ById" + id);	
		vouchertypeService.deleteVoucherTypebyId(id);
		return "redirect:/VoucherType";
	}

	@RequestMapping(path = "/NewVouchertype", method = RequestMethod.POST)
	public String createOrUpdateVoucherType(VoucherType vouchertype) 
	{
		System.out.println("createOrUpdateVoucherType ");
		vouchertypeService.createOrUpdateVoucherType(vouchertype);
		return "redirect:/VoucherType";
	}

}
