package com.diginepal.schoolmgmt.service;

import java.util.List;

import com.diginepal.schoolmgmt.model.VoucherType;

public interface VouchertypeService {
	List<VoucherType> getallVoucherTypes();
	//void saveVoucherTypes(VoucherType voucherType);
	VoucherType getVoucherTypeById(long id);
	void deleteVoucherTypebyId(long id);
	void createOrUpdateVoucherType(VoucherType vouchertype);
}
