package arn.accountfinal.model;

public enum AccountType{
	Assets,
	Liabilities,
	Equity,
	Revenue,
	Expenses
}
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
//
//@Entity
//@Table(name = "accounttype")
//public class AccountType {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//   //unique = true, nullable=false 
//    @Column(name = "Acc_type",nullable=false)
//    private String Acc_type;
//    
//    @Column(name = "description",length = 1024)
//    private String description;
//    
//    public AccountType(String acc_type, String description) {
//		super();
//		Acc_type = acc_type;
//		this.description = description;
//	}
//    
//    
//	public AccountType() {
//		// TODO Auto-generated constructor stub
//	}
//
//
//	public String getDescription() {
//		return description;
//	}
//	public void setDescription(String description) {
//		this.description = description;
//	}
//	public String getAcc_type() {
//		return Acc_type;
//	}
//	
//	public void setAcc_type(String acc_type) {
//		Acc_type = acc_type;
//	}
//}