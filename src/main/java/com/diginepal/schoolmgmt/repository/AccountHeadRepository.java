package com.diginepal.schoolmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.diginepal.schoolmgmt.model.AccountHead;

@Repository

public interface AccountHeadRepository extends JpaRepository<AccountHead, Long> {

		
	@Query("select ud from tbl_accounthead ud where ud.id=?1")
	public List<AccountHead> findBySubcode(@Param("id") String id);
	
	@Query("select ud from tbl_accounthead ud where ud.id=?1")
	public List<AccountHead> findByGroupcode(@Param("id") String id);

}
