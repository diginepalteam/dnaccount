package arn.accountfinal.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import arn.accountfinal.model.AccountHead;
import arn.accountfinal.model.Transaction;
import arn.accountfinal.model.VoucherType;
import arn.accountfinal.service.AccountHeadService;
import arn.accountfinal.service.TransactionService;
import arn.accountfinal.service.VouchertypeService;

@Controller
public class TransactionControl {

@Autowired
	private TransactionService transactionService;
@Autowired
private AccountHeadService accountHeadService;

@Autowired
private VouchertypeService vouchertypeService;


// display list of Account Heads
    @GetMapping("/Trans")
    public String viewHomePage(Model model) {
    	        
//        List<AccountType> accountTypes=accservice.getallAccountHeads();
//        model.addAttribute("accountTypes",accountTypes);
        
//        
//     // create model attribute to bind form data
//        AccountHead accountHead = new AccountHead();
//        model.addAttribute("accountHead", accountHead);
//        
//        //Get Account Type 
//        accountHead.getCategory_Head();
//        model.addAttribute("acchead",AccountHead.values());
        
//        System.out.println("list"+accountTypes); 
    	List<AccountHead> accountHead = new ArrayList<>();
        model.addAttribute("accountHead", accountHead);
        List<AccountHead> accountHeads = accountHeadService.getallAccountHeads();
        model.addAttribute("accountHeads", accountHeads);
        System.out.println("list of account head"+accountHeads);
        
//        For Voucher Type Load in Select Tag
        List<VoucherType> voucherType = new ArrayList<>();
        model.addAttribute("voucherType", voucherType);
        List<VoucherType>voucherTypes=vouchertypeService.getallVoucherTypes();
        model.addAttribute("voucherTypes", voucherTypes);
        System.out.println("list of account head"+voucherTypes);
        return "voucherentry";
    }
 
    @GetMapping("/NewTransactionForm")
    public String showNewTransactionForm(Model model) {
        // create model attribute to bind form data
        Transaction transaction = new Transaction();
        model.addAttribute("transaction", transaction);
       // System.out.println("list"+accountHead.getAccount_Head());
        return "voucherentry";
    }
    
    @PostMapping("/saveTransaction")
    public String saveAccountTransaction(@ModelAttribute("transaction") Transaction transaction) {
        // save employee to database
    	transactionService.saveTransactions(transaction);
        return "redirect:/voucherentry";
    }
   
//    @GetMapping("/transactionForUpdate/{id}")
//	public String showFormForUpdate(@PathVariable ( value = "id") long id, Model model) {
//	 
//	 // get employee from the service
//    	Transaction transaction = transactionService.getTransactionById(id);
//	  //System.out.println("Id"+id);
//	  //System.out.println("list out arun"+accountheadservice.getAccountHeadById(id));
//	 // set employee as a model attribute to pre-populate the form
//	 model.addAttribute("transaction", transaction);
//	 return "voucherentry";
//	}
//   
//    @GetMapping("/transactionForDelete/{id}")
//   	public String showFormForDelete(@PathVariable ( value = "id") long id, Model model) {
//   	 
//   	 // Call delete method from the service
//   	
//   	this.transactionService.deleteTransactionbyId(id);
//   	 return "redirect:/voucherentry";
//   	}
//    
	
}
